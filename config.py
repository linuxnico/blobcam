#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = '1'

# a rensigner!!!
dossierD = '/home/blob/'
# a rensigner!!!

staticD = './static/'
gifD = f'{staticD}gifs/'
imageD = f'{staticD}images/'
thumbD = f'{staticD}thumbs/'
debug = 1
delais = 60
dossierSauvegarde = f"{dossierD}{imageD}"
dossier_thumb = f'{dossierD}{thumbD}'
colonnes = 5
size = (128, 128)
basedossier = f'{dossierD}{staticD}'

fichierLog = f"{dossierD}log/log.log"
