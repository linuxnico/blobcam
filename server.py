#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = '1'

from config import *
import Images
# from sys import exit
import os
from datetime import datetime
try:
    from flask import Flask, request, render_template, url_for
except Exception:
    logger.warning("vous devez installer Flask: pip install Flask")
    logger.warning("Vous devez installer Pillow: pip install Pillow")

app = Flask(__name__)
app.secret_key = '12155AAAAB43543135435432135453F'
import glob
# import
try:
    from PIL import Image
except Exception:
    logger.warning("Vous devez installer Pillow: pip install Pillow")

# from datetime import datetime
from loguru import logger
from sys import stdout
NIVEAULOG = debug
logger.remove(None)

logger.add(fichierLog,
           format='<green>{time:DD-MM-YYYY HH:mm:ss.SSS}</green> | <level>{level: <8}</level> | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level>{message}</level>',
           rotation="1 MB",
           level=NIVEAULOG)
logger.add(stdout,
           format='<green>{time:DD-MM-YYYY HH:mm:ss.SSS}</green> | <level>{level: <8}</level> | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level><white>{message}</white></level>',
           level=NIVEAULOG)
date_Heure = str(datetime.now().day).zfill(2) + "-" + str(datetime.now().month).zfill(2) + "-" + str(datetime.now().year) + "_" + str(datetime.now().hour) + "-" + str(datetime.now().minute) + "-" + str(datetime.now().second)
heure_debut = 'Debut du script... le ' + date_Heure
logger.info(heure_debut)


def creategif():
    date_jour = str(datetime.now().year)+str(datetime.now().month).zfill(2)+str(datetime.now().day).zfill(2)
    fichiergif = url_for('static', filename=f'gifs/{date_jour}.gif')[1:]
    images = glob.glob(url_for('static', filename=dossier+f'/{date_jour}')[1:]+'*.png')
    logger.debug(images)
    im = Image.new(mode="RGB", size=Image.open(images[0]).size)
    appendimage = []
    for i in images:
        appendimage.append(Image.open(i))
    logger.debug(images)
    logger.debug(fichiergif)
    im.save(fichiergif, save_all=True, append_images=appendimage, loop=0, duration=40)
    return fichiergif


def decoupeColonne(tab, col):
    nbr = len(tab)
    indice = 0
    restab = []
    while indice < nbr:
        # print(indice)
        restab.append(tab[indice:indice+col])
        indice += col
        # print('indice+col:', indice)
        if indice+col > nbr:
            col = nbr-indice
        # print('col: ', col)
    return restab


def getGif(folder=f'{gifD}*.gif'):
    fichier = []
    for g in glob.glob(folder):
        date = os.path.basename(g).split('.')[0]
        dateMEF = date[-2:] + '/' + date[-4:-2] + '/' + date[:4]
        fichier.append([os.path.basename(g), dateMEF, os.path.basename(glob.glob(f'{thumbD}{date}10*.png')[0])])
    fichiers = sorted(fichier, key=lambda x: (x[1]))
    # fichiers = decoupeColonne(fichiers, colonnes)
    logger.debug(f"fichiers gifs: {fichiers}")
    return fichiers


def getImage(folder, date=''):
    tabImage = []
    for im in glob.glob(folder):
        fichier = os.path.basename(im)
        miniature = fichier[:-4]+'_thumb.png'
        date = fichier[:8]
        tabImage.append(Images.Image(fichier, miniature, date))
    logger.debug(tabImage)
    return tabImage


def arrondi(num, div):
    if num / div != num % div:
        return num % div + 1
    return int(num / div)


@app.route('/gif', methods=['POST', 'GET'])
def gif():
    if request.method == 'POST':
        name = request.form.get('gif', '')
    else:
        name = 'gif' in request.args or ''
    # print(name)
    if name == '':
        ret = getGif()
        return render_template('indexGifs.html', title="Blob's Gifs", fichiers=ret, colonnes=colonnes)

    else:
        creategif()
        ret = getGif()
        return render_template('indexGifs.html', title="Blob's Gifs", fichiers=ret, colonnes=colonnes)

        # return send_file(ret, mimetype='image/gif')
    # return ret


@app.route('/', methods=['POST', 'GET'])
def index():
    fichiers = getImage(f'{imageD}*.png')
    # print(fichiers)
    return render_template('index.html', title="Blob's Images", fichiers=fichiers, colonnes=colonnes)


# if __name__ == "__main__":
logger.warning("*******demarrage du serveur a {}!".format(datetime.now()))
getImage(f'{imageD}/*.png')
open('res.txt', 'a').write(str(datetime.now())+'\n')
app.run(host='0.0.0.0', port=8000, debug=False, use_reloader=False)
