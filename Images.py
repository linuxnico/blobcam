#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = '1'

from config import *
from datetime import datetime
from os.path import isfile, basename
from PIL import Image as pilimage


class Image():
    def __init__(self, fichier, miniature, date):
        self.fichier = fichier
        self.miniature = miniature
        self.date = date
        self.dateMEF = self.date[-2:] + '/' + self.date[-4:-2] + '/' + self.date[:4]
        self.creaMiniature()

    def __repr__(self):
        return f'en date du {self.date}, a ete prise la photo {self.fichier}, miniature {self.miniature}'

    def dujour(self, date=''):
        date_jour = str(datetime.now().year)+str(datetime.now().month).zfill(2)+str(datetime.now().day).zfill(2)
        if self.date == date_jour:
            return True
        return False

    def creaMiniature(self, dossier='thumbs'):
        # print(self.fichier, self.miniature)
        if not isfile(self.miniature):
            thumbfile = basedossier + dossier + '/' + basename(self.fichier)[:-4] + '_thumb.png'
            im = pilimage.open(basedossier + 'images' + '/' + self.fichier)
            im.thumbnail(size)
            im.save(thumbfile, "PNG")


if __name__ == '__main__':
    tes = Image('./static/images/20220210100023.png', 'fichier_miniature', '20220207')
    print(tes)
    print(tes.dateMEF)
    print(tes.dujour())
