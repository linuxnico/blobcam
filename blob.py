#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = '1'

from config import *

# constantes

import os
if not os.path.isdir(dossierSauvegarde):
    os.mkdir(dossierSauvegarde)
if not os.path.isdir(dossier_thumb):
    os.mkdir(dossier_thumb)

# import
try:
    from PIL import Image
except Exception:
    logger.warning("Vous devez installer Pillow: pip install Pillow")

import time
import glob
from datetime import datetime
from loguru import logger
from sys import stdout
NIVEAULOG = debug
logger.remove(None)
logger.add(fichierLog,
           format='<green>{time:DD-MM-YYYY HH:mm:ss.SSS}</green> | <level>{level: <8}</level> | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level>{message}</level>',
           rotation="1 MB",
           level=NIVEAULOG)
logger.add(stdout,
           format='<green>{time:DD-MM-YYYY HH:mm:ss.SSS}</green> | <level>{level: <8}</level> | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level><white>{message}</white></level>',
           level=NIVEAULOG)
date_Heure = str(datetime.now().day).zfill(2) + "-" + str(datetime.now().month).zfill(2) + "-" + str(datetime.now().year) + "_" + str(datetime.now().hour) + "-" + str(datetime.now().minute) + "-" + str(datetime.now().second)
heure_debut = 'Debut du script... le ' + date_Heure
logger.info(heure_debut)


def tabTemp(fichiers):
    res = []
    for f in fichiers:
        logger.debug(f)
        res.append(int(open(f).read().split("t=")[1].strip())/1000)
    logger.debug(res)
    return res


def thumbnails():
    images = glob.glob(dossierSauvegarde+'/*.png')
    thumbs = [os.path.basename(i)[:-4] for i in glob.iglob(dossier_thumb+'/*.png')]
    logger.debug(images)
    logger.debug(thumbs)
    for image in images:
        if os.path.basename(image)[:-4] not in thumbs:
            thumbfile = dossier_thumb + '/' + os.path.basename(image)[:-4] + '_thumb.png'
            im = Image.open(image)
            im.thumbnail(size)
            im.save(thumbfile, "PNG")


def main():
    capteursTemperatures = glob.glob("/sys/bus/w1/devices/28*/w1_slave")
    capteursTemperatures.sort()
    logger.debug(f'capteurs: {capteursTemperatures}')
    from subprocess import run, DEVNULL
    ancien_fichier = None
    while True:
        auj = datetime.now()
        fichier_prisedevue = f"{auj.year:02}{auj.month:02}{auj.day:02}{auj.hour:02}{auj.minute:02}{auj.second:02}.png"

        # on prend sune photos
        logger.debug('on prends la photo')
        try:
            run(['libcamera-still', '-e', 'png', '--rotation', '180', '-o', dossierSauvegarde+"/"+fichier_prisedevue], stdout=DEVNULL)
            ancien_fichier = fichier_prisedevue
        except Exception:
            if ancien_fichier is not None:
                os.copy(ancien_fichier, fichier_prisedevue)
                logger.warning('Erreur: {fichier_prisedevue}')

        # on cree la miniature
        logger.debug('on cree la miniature')
        thumb = dossier_thumb + '/' + fichier_prisedevue[:-4] + '_thumb.png'
        try:
            :
            im = Image.open(dossierSauvegarde+"/"+fichier_prisedevue)
            im.thumbnail(size)
            im.save(thumb, "PNG")
        except IOError:
            logger.warning("cannot create thumbnail for", i)

        # on prends les temperatures
        logger.debug('on prends les temperatures')
        temps = tabTemp(capteursTemperatures)

        # on ecrit le logg
        logger.debug('on ecrit les infos')
        logger.info(f"{fichier_prisedevue} ,temp: {temps}")

        # on attends la prochaine photo
        logger.debug('on attends')
        time.sleep(delais)


if __name__ == '__main__':
    thumbnails()
    main()
    # thumbnails()
